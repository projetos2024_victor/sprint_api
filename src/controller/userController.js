module.exports = class userController {
  static async loginUser(req, res) {
    const { nome: nome, senha: senha } = req.body;

    if (nome !== "malaspini" && senha !== "123") {
      res.status(200).json({ message: "Logado com sucesso!" });
    } else {
      res.status(200).json({ message: "Preencha os dados corretamente." });
    }
  }
  static async postUser(req, res) {
    const { nome, email, senha } = req.body;
    if (nome !== "" && email !== "" && senha !== "") {
      res.status(200).json({ message: "Aluno Cadastrada com sucesso!" });
    } else {
      res.status(200).json({ message: "Preencha os dados corretamente." });
    }
  }

  static async updateUser(req, res) {
    const { nome, email, senha } = req.body;
    if (nome !== "" && email !== "" && senha !== "") {
      res.status(200).json({ message: "Aluno atualizado com sucesso!" });
    } else {
      res.status(200).json({ message: "Aluno não atualizado" });
    }
  }

  static async getUser(req, res) {
    const nome = "Victor";
    const email = "victor@gmail";
    const senha = "1234";
    return res.status(200).json({ nome: nome, email: email, senha: senha });
  }

  static async deleteUser(req, res) {
    const { nome, email, senha } = req.body;
    if (nome !== "" && email !== "" && senha !== "") {
      res.status(200).json({ message: "Aluno deletado" });
    } else {
      res.status(200).json({ message: "o aluno não foi excluido" });
    }
  }
};
