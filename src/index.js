const express = require('express');
// Importa o framework Express, que será utilizado para criar e gerenciar o servidor web
const app = express();
// Cria uma instância do aplicativo Express

const cors = require('cors');
// Importa o módulo CORS, que é um middleware para permitir requisições de diferentes origens (Cross-Origin Resource Sharing)

class AppController {
// Define a classe AppController

    constructor() {
    // Define o construtor da classe

      this.express = express();
      // Cria uma instância do Express e armazena em this.express

      this.middlewares();
      // Chama o método middlewares para configurar os middlewares do Express

      this.routes();
      // Chama o método routes para configurar as rotas do Express
    }

    middlewares() {
    // Define o método middlewares

      this.express.use(express.json());
      // Usa o middleware express.json() para fazer o parsing de requisições com formato JSON

      this.express.use(cors());
      // Usa o middleware CORS para permitir requisições de diferentes origens
    }

    routes() {
    // Define o método routes

      const apiRoutes= require('./routes/apiRoutes')
      // Importa as rotas definidas no arquivo apiRoutes.js

      this.express.use('/teste/', apiRoutes)
      // Define o prefixo '/teste/' para as rotas importadas de apiRoutes

      this.express.get('/teste/', (_, res) => {
        res.send('api conectada')
      })
      // Define uma rota GET para '/teste/' que retorna uma mensagem indicando que a API está conectada
    }
  }

  module.exports = new AppController().express;
  // Exporta uma instância do Express criada pela classe AppController
