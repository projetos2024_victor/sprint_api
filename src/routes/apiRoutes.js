const router = require('express').Router()
const userController = require('../controller/userController');

router.post('/user/',userController.postUser)
router.post('/createUser/',userController.loginUser)
router.put('/userUpdate/', userController.updateUser)
router.delete('/userDelete/', userController.deleteUser)
router.get('/getUser/', userController.getUser)

module.exports = router;